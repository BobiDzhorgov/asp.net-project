# Library Management System
![](logo.jpg)
## Team
- Diyan Lyubchev *(lead)*  
- Boycho Dzhorgov
## Objective
### Use Microsoft SQL Server
## Task Board
[Trello](https://trello.com/b/Y7flRM29)
## Instructions
When you start the project you have a main menu in which you can choose one the options given.

The menu works on the principle by choosing an option using the Up and Down keys from the keyboard. 

And here are the options:
- Login (You can enter the library system if you already have an account)
- Register (If you do not have an account you have to register to have access to the library)
- Exit 
      
When you login you have options from which you can choose depending on you account type (admin/member).

When you register your account by default is of type member. Only the admin can make you an admin too.

If you are an admin you can choose among:
1. AddBook (You can add a new book in the library system)
2. EditBook (You can edit an existing book)
3. EdinUsers (You can edit an existing user)
4. ReserveBook 
5. CheckOutBook 
6. RegisterBook
7. RenewBook (You can extend the period when the book is in you)
8. ReturnBook
9. SearchInCatalog(You can either search a book by its title/author/subject/publish date)
10. Logout (You return to the main menu)
11. Quit (Exits the program)
    
If you are a member you can choose among:
1. ReserveBook 
2. CheckOutBook 
3. RenewBook (You can extend the period when the book is in you)
4. ReturnBook
5. SearchInCatalog(You can either search a book by its title/author/subject/publish date)
6. Logout (You return to the main menu)
7. Quit (Exits the program)

## Customer
### Vennesla Library and Cultural Center, Vennesla, Norway
This library in Norway is made of a series of arcs, or "ribs," that support the roof. The concept of the building comes from the ribs of a whale skeleton.
"In this project, we developed a rib concept to create usable hybrid structures that combine a timber construction with all technical devices and the interior," architects Helen & Hard told ArchDaily.

