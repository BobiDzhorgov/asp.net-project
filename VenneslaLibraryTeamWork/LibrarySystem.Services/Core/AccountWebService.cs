﻿using LibrarySystem.DbContext;
using LibrarySystem.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibrarySystem.Services.Core
{
    public class AccountWebService : IAccountWebService
    {
        private readonly LibrarySystemContext dbContext;

        public AccountWebService(LibrarySystemContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public User BanAccount(string username)
        {
            var user = this.dbContext.Users
                .FirstOrDefault(userName => userName.UserName == username);

            if (user == null)
            {
                throw new Exception($"User with the follwing {username} does not exist!");
            }
            user.IsBanned = true;

            this.dbContext.SaveChanges();

            return user;


        }
    }
}
