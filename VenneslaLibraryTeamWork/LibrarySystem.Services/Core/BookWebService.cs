﻿using LibrarySystem.Books;
using LibrarySystem.Contracts.Books;
using LibrarySystem.DbContext;
using LibrarySystem.Models.Books;
using LibrarySystem.Services.Contracts.System;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibrarySystem.Services.Core
{
    public class BookWebService : IBookWebService
    {
        private readonly LibrarySystemContext dbContext;

        public ISystemService SystemService { get; private set; }

        public BookWebService(ISystemService systemService, LibrarySystemContext dbcontext)
        {
            SystemService = systemService;
            this.dbContext = dbcontext;
        }


        public async Task<IEnumerable<IBook>> GetAllBooks()
        {
            return await this.dbContext.Books.ToListAsync();
        }
        public async Task<Book> CreateBookAsync(string isbn, string title, int pages, string subject,
           string publishers, string publishDate, string author)
        {
            var book = new Book
            {
                ISBN = isbn,
                Title = title,
                Pages = pages,
                Subject = subject,
                Publishers = publishers,
                PublishDate = publishDate,
                Author = author
            };
            if (isbn != null || title != null)
            {
                this.dbContext.Books.Add(book);
                await dbContext.SaveChangesAsync();
            }

            return book;

        }
        public async Task<IBook> FindBook(int id)
        {
            return await this.dbContext.Books
                 .FirstOrDefaultAsync(book => book.Id == id);
        }
        public async Task RemovedBookAsync(string isbn)
        {
            var book = await dbContext.Books
                .FirstOrDefaultAsync(bookIsbn => bookIsbn.ISBN == isbn);

            if (book == null)
            {
                throw new Exception("The book was not found!");
            }

            if (book != null)
            {
                this.dbContext.Books.Remove(book);
                await this.dbContext.SaveChangesAsync();
            }

        }
        public async Task<IBook> SearchBook(string title)
        {
            var bookSearch = await this.dbContext.Books
                .FirstOrDefaultAsync(book => book.Title == title);
            return bookSearch;
        }
        public void LendBook(string title, string userName)
        {
            var book = this.dbContext.Books.FirstOrDefault(bTitle => bTitle.Title == title);
            var user = this.dbContext.Users
                .Include(us => us.BookLendings)
                .FirstOrDefault(uLend => uLend.UserName == userName);

            if (book == null)
            {
                throw new Exception("The book was not found!");
            }
            if (user.CountLendBooks < 1)
            {
                throw new Exception("You cannot take more than 3 books!");
            }


            var bookLending = this.dbContext.BookLendings
                .FirstOrDefault(bLending => bLending.BookId == book.Id
                    && !bLending.ReturnDate.HasValue
                    && bLending.Date.AddDays(10) > DateTime.Now);    // This is enshures automatic return after 10 days


            if (bookLending != null)
            {
                throw new Exception("The book has already been lended!");
            }

            bookLending = new BookLending()
            {

                Book = book,
                User = user,
                Date = DateTime.Now
            };
            user.CountLendBooks -= 1;


            var bookReservation = this.dbContext.BookReservations
                .Include(item => item.Notification)
                .FirstOrDefault(item => item.BookId == book.Id && item.UserId == user.Id && item.Active);

            if (bookReservation != null)
            {
                bookReservation.Active = false;
                bookReservation.Notification.Seen = true;
            }

            this.dbContext.BookLendings.Add(bookLending);
            this.dbContext.SaveChanges();
        }

        public bool ReserveBook(string title, string userName)
        {
            var book = this.dbContext.Books.FirstOrDefault(bTitle => bTitle.Title == title);
            var user = this.dbContext.Users
                .Include(us => us.BookReservations)
                .FirstOrDefault(uLend => uLend.UserName == userName);

            if (book == null)
            {
                return false;
            }
            var bookReservation = new BookReservation()
            {

                Book = book,
                User = user,
                Date = DateTime.Now
            };


            this.dbContext.BookReservations.Add(bookReservation);
            this.dbContext.SaveChanges();
            return true;
        }
        public void ReturnBook(string isbn, string userName)
        {
            var book = this.dbContext.Books.FirstOrDefault(bTitle => bTitle.ISBN == isbn);

            var user = this.dbContext.Users
                .Include(us => us.BookReservations)
                .FirstOrDefault(uLend => uLend.UserName == userName);

            if (book == null)
            {
                throw new Exception("Book not found!");
            }
            if (user.CountLendBooks > 2)
            {
                throw new Exception("You have no more books for return");
            }


            var bookLending = this.dbContext.BookLendings
                .FirstOrDefault(bLending => bLending.BookId == book.Id && bLending.UserId == user.Id);

            if (bookLending == null)
            {
                throw new Exception("The book has not been lended to the current user.");
            }

            bookLending.ReturnDate = DateTime.Now;     // manual return
            user.CountLendBooks += 1;

            this.dbContext.BookLendings.Remove(bookLending);  /////////////////
            this.dbContext.SaveChanges();
        }
        public void RenewBook(string isbn, string username, int days)
        {
            var book = this.dbContext.Books.
                FirstOrDefault(bRenew => bRenew.ISBN == isbn);

            if (book == null)
            {
                throw new Exception($"There is no book with the following ISBN {isbn}.");
            }

            var user = this.dbContext.Users
                .Include(uLend => uLend.BookLendings)
                .FirstOrDefault(uRenewBook => uRenewBook.UserName == username);

            var bookLending = this.dbContext.BookLendings
                .FirstOrDefault(bLending => bLending.BookId == book.Id
                && bLending.UserId == user.Id);

            if (bookLending == null)
            {
                throw new Exception("The book has not beed lended by the current user.");
            }

            if (days > 10)
            {
                throw new Exception("You cannot renew book for more than 10 days.");
            }

            bookLending.Date.AddDays(days);

            this.dbContext.SaveChanges();
        }
    }
}
