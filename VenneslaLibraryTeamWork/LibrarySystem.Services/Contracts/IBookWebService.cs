﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LibrarySystem.Contracts.Books;
using LibrarySystem.Models.Books;
using LibrarySystem.Services.Contracts.System;

namespace LibrarySystem.Services.Core
{
    public interface IBookWebService
    {
        void ReturnBook(string isbn, string userName);
        ISystemService SystemService { get; }
        Task<IBook> SearchBook(string title);
        Task<Book> CreateBookAsync(string isbn, string title, int pages, string subject,
           string publishers, string publishDate, string author);
        Task<IBook> FindBook(int id);
        Task<IEnumerable<IBook>> GetAllBooks();
        Task RemovedBookAsync(string isbn);
        void LendBook(string title, string userName);
        bool ReserveBook(string title, string userName);
        void RenewBook(string isbn, string username, int days);
    }
}