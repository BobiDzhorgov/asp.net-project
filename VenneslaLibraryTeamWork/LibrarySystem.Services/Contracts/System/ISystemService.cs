﻿using LibrarySystem.Books;
using System.Collections.Generic;

namespace LibrarySystem.Services.Contracts.System
{
    public interface ISystemService
    {
        decimal CalculateFine(BookLending bookLending);
        bool ValidateLendPeriod(BookLending bookLending);
    }
}