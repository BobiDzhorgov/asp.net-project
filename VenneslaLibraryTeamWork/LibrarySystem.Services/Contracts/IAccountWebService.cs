﻿using LibrarySystem.Models.Accounts;

namespace LibrarySystem.Services.Core
{
    public interface IAccountWebService
    {
        User BanAccount(string username);
    }
}