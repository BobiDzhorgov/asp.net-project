﻿using LibrarySystem.Accounts;
using LibrarySystem.Books;
using LibrarySystem.Models.Accounts;
using LibrarySystem.Models.Books;
using LibrarySystem.Models.Notifications;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LibrarySystem.DbContext
{
    public class LibrarySystemContext : IdentityDbContext<User>
    {

        public LibrarySystemContext()
        {
        }
        public LibrarySystemContext(DbContextOptions<LibrarySystemContext> options)
          : base(options)
        {
        }
        public DbSet<RoleUser> RoleUsers { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<BookReservation> BookReservations { get; set; }
        public DbSet<BookLending> BookLendings { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                const string connectionString =
          @"Server=.\SQLEXPRESS;Database=LibraryDatabase;Trusted_Connection=True;";

                optionsBuilder.UseSqlServer(connectionString);
            }
          
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // UserRole
           // modelBuilder.Entity<RoleUser>().HasKey(role => role.Id);

         //  modelBuilder.Entity<UserRole>()
         //      .HasOne(user => user.User)
         //      .WithOne(user => user.UserRole);
         //
         //  modelBuilder.Entity<User>()
         //      .HasOne(role => role.UserRole)
         //      .WithOne(role => role.User);

            //Book
            modelBuilder.Entity<Book>()
                .Property(book => book.ISBN)
                .HasColumnType("nvarchar(15)")
                .IsRequired();

            modelBuilder.Entity<Book>()
               .Property(book => book.ISBN)
               .HasColumnType("nvarchar(15)")
               .IsRequired();

            modelBuilder.Entity<Book>()
               .Property(book => book.Title)
               .HasColumnType("nvarchar(100)")
               .IsRequired();

            modelBuilder.Entity<Book>()
             .Property(book => book.Subject)
             .HasColumnType("nvarchar(1000)")
             .IsRequired();

            modelBuilder.Entity<Book>()
             .Property(book => book.PublishDate)
             .HasColumnType("nvarchar(50)")
             .IsRequired();

            modelBuilder.Entity<Book>()
             .Property(book => book.Publishers)
             .HasColumnType("nvarchar(30)")
             .IsRequired();

            modelBuilder.Entity<Book>()
             .Property(book => book.Author)
             .HasColumnType("nvarchar(30)")
             .IsRequired();

            base.OnModelCreating(modelBuilder);
        }
    }
}
