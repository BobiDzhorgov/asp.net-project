﻿using LibrarySystem.BaseEntitys;
using LibrarySystem.Books;
using LibrarySystem.Contracts.Books;
using LibrarySystem.Models.Notifications;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibrarySystem.Models.Books
{
    public class Book : IBook      // Single Responsibility
    {
        
        [Key]
        public int Id { get; set; }

        public virtual List<BookReservation> BookReservations { get; set; }

        public virtual List<BookLending> BookLendings { get; set; }

    

        public string ISBN { get; set; }

        public string Title { get; set; }
        [Required]
        [Range(20, 2000, ErrorMessage = "Pages must be between 20 and 2000.")]
        public int Pages { get; set; }

        public string Subject { get; set; }

        public string Publishers { get; set; }

        public string PublishDate { get; set; }

        public string Author { get; set; }

        [NotMapped]
        public string LastNotification { get; set; }

        public override string ToString()
        {
            return $"Title: {Title}";
        }

    }
}