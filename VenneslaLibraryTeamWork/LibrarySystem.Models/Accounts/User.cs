﻿using LibrarySystem.Accounts;
using LibrarySystem.Books;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace LibrarySystem.Models.Accounts
{
    public class User : IdentityUser
    {
        public User()
        {
            this.BookLendings = new List<BookLending>();
            this.BookReservations = new List<BookReservation>();
        }
    
        public ICollection<BookLending> BookLendings { get; }

        public ICollection<BookReservation> BookReservations { get; }
        public bool IsBanned { get; set; }
        [NotMapped]
        public string LastNotification { get; set; }

        public int CountLendBooks { get; set; } = 3;

    }
}
