﻿using LibrarySystem.Accounts;
using LibrarySystem.DbContext;
using LibrarySystem.Models.Accounts;
using LibrarySystem.Models.Books;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibrarySystem.Web.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static async Task SeedDatabaseRoleAsync(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<LibrarySystemContext>();
                context.Database.Migrate();

                var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<RoleUser>>();
                var userManager = serviceScope.ServiceProvider.GetService<UserManager<User>>();

                await Task.Run(async () =>
                {

                    var adminRole = "Admin";

                    var exists = await roleManager.RoleExistsAsync(adminRole);

                    if (!exists)
                    {
                        await roleManager.CreateAsync(new RoleUser
                        {
                            Name = adminRole
                        });
                    }

                    var AdminNameD = "Diyan";

                    var user = await userManager.FindByNameAsync(AdminNameD);

                    if (user == null)
                    {
                        user = new User
                        {
                            UserName = AdminNameD,
                            Email = "admin@admin.com"
                        };

                        await userManager.CreateAsync(user, "123456");
                        await userManager.AddToRoleAsync(user, adminRole);
                    }
                    var AdminNameB = "Bobi";


                    user = await userManager.FindByNameAsync(AdminNameB);

                    if (user == null)
                    {
                        user = new User
                        {
                            UserName = AdminNameB,
                            Email = "admin@admin.com"
                        };

                        await userManager.CreateAsync(user, "234567");
                        await userManager.AddToRoleAsync(user, adminRole);
                    }

                    var book1 = new Book
                    {
                        ISBN = "9781593275846",
                        Title = "Assassin's creed",
                        Author = "Oliver Bowden",
                        PublishDate = "2011-11-10",
                        Publishers = "Era",
                        Pages = 300,
                        Subject = "Romance",
                    };
                    if (!context.Books.Any(bookdb => bookdb.ISBN == book1.ISBN))
                    {
                        await context.Books.AddAsync(book1);
                    }

                    var book2 = new Book
                    {
                        ISBN = "9784445814481",
                        Title = "Angels & Demons",
                        Author = "Dan Brown",
                        PublishDate = "2000-11-10",
                        Publishers = "Soft Press",
                        Pages = 616,
                        Subject = "Thriller",
                    };
                    if (!context.Books.Any(bookdb => bookdb.ISBN == book2.ISBN))
                    {
                        await context.AddAsync(book2);
                    }

                    var book3 = new Book
                    {
                        ISBN = "9785213275846",
                        Title = "The chalk man",
                        Author = "C. J. Tudor   ",
                        PublishDate = "2018-01-09",
                        Publishers = "Colibri",
                        Pages = 309,
                        Subject = "Thriller",
                    };
                    if (!context.Books.Any(bookdb => bookdb.ISBN == book3.ISBN))
                    {
                        await context.Books.AddAsync(book3);
                    }

                    var book4 = new Book
                    {
                        ISBN = "9782593321154",
                        Title = "Child 44",
                        Author = "Tom Rob Smith",
                        PublishDate = "2008-11-10",
                        Publishers = "Lachezar Minchev",
                        Pages = 459,
                        Subject = "Thriller",
                    };
                    if (!context.Books.Any(bookdb => bookdb.ISBN == book4.ISBN))
                    {
                        await context.Books.AddAsync(book4);
                    }

                    var book5 = new Book
                    {
                        ISBN = "978993111146",
                        Title = "It",
                        Author = "Stephen King",
                        PublishDate = "1986-09-15",
                        Publishers = "Era",
                        Pages = 1138,
                        Subject = "Horror novel",   
                    };
                    if (!context.Books.Any(bookdb => bookdb.ISBN == book5.ISBN))
                    {
                        await context.Books.AddAsync(book5);
                    }

                    var book6 = new Book
                    {
                        ISBN = "9786190204725",
                        Title = "The good daughter",
                        Author = "Karin Slaughter",
                        PublishDate = "2017-06-18",
                        Publishers = "Colibri",
                        Pages = 650,
                        Subject = "Thriller",
                    };
                    if (!context.Books.Any(bookdb => bookdb.ISBN == book6.ISBN))
                    {
                        await context.Books.AddAsync(book6);
                    }

                    var book7 = new Book
                    {
                        ISBN = "9789545853418",
                        Title = "Kane and Abel",
                        Author = "Jeffrey Archer",
                        PublishDate = "2010-02-20",
                        Publishers = "Bard",
                        Pages = 579,
                        Subject = "Thriller",
                    };
                    if (!context.Books.Any(bookdb => bookdb.ISBN == book7.ISBN))
                    {
                        await context.Books.AddAsync(book7);
                    }

                    var book8 = new Book
                    {
                        ISBN = "9789545855283",
                        Title = "The prodigal daughter",
                        Author = "Jeffrey Archer",
                        PublishDate = "2008-05-08",
                        Publishers = "Bard",
                        Pages = 479,
                        Subject = "Novel",
                    };
                    if (!context.Books.Any(bookdb => bookdb.ISBN == book8.ISBN))
                    {
                        await context.Books.AddAsync(book8);
                    }

                    var book9 = new Book
                    {
                        ISBN = "9788387834111",
                        Title = "The notebook",
                        Author = "Nicholas Sparks",
                        PublishDate = "1996-10-01",
                        Publishers = "Warner Books",
                        Pages = 214,
                        Subject = "Novel",
                    };
                    if (!context.Books.Any(bookdb => bookdb.ISBN == book9.ISBN))
                    {
                        await context.Books.AddAsync(book9);
                    }

                    var book10 = new Book
                    {
                        ISBN = "9780785941743",
                        Title = "The Da Vinci Code",
                        Author = "Dan Brown",
                        PublishDate = "2000-11-24",
                        Publishers = "Bantam Books",
                        Pages = 689,
                        Subject = "Mystery thriller",
                    };
                    if (!context.Books.Any(bookdb => bookdb.ISBN == book10.ISBN))
                    {
                        await context.Books.AddAsync(book10);
                    }                    

                    await context.SaveChangesAsync();
                });
            }
        }
    }
}
