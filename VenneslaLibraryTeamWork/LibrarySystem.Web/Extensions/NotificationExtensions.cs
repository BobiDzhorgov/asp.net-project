﻿using LibrarySystem.Books;
using LibrarySystem.DbContext;
using LibrarySystem.Models.Notifications;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibrarySystem.Web.Extensions
{
    public static class NotificationExtensions
    {
        public static void SyncNotifications(this LibrarySystemContext context, string username)
        {
            var bookReservations = context.BookReservations
                .Include(item => item.Book)
                .Include(item => item.User)
                .Where(item => item.Active
                    && item.User.UserName == username
                    && item.NotificationId == null)
                .ToList();

            var notificationReservations = new List<BookReservation>();

            foreach (var reservation in bookReservations)
            {
                var isTaken = context.BookLendings
                    .Include(item => item.Book)
                    .Include(item => item.User)
                    .Any(item => item.BookId == reservation.BookId
                        && !item.ReturnDate.HasValue
                        && item.Date.AddDays(10) > DateTime.Now);

                if (!isTaken)
                {
                    notificationReservations.Add(reservation);
                }
            }

            if (notificationReservations.Any())
            {
                context.Notifications.AddRange(notificationReservations.Select(item => new Notification { BookReservation = item }));
                context.SaveChanges();
            }

        }

        public static string GetLastNotification(this DbSet<Notification> notifications, string username)
        {
            var lastNotification = notifications
                .Include(notification => notification.BookReservation)
                .Include(notification => notification.BookReservation.Book)
                .Include(notification => notification.BookReservation.User)
                .Where(notification => notification.BookReservation.User.UserName == username
                    && !notification.Seen
                    && notification.BookReservation != null
                    && notification.BookReservation.Active)
                .OrderByDescending(notification => notification.BookReservation.Date)
                .FirstOrDefault();

            return lastNotification != null ? $"{lastNotification.BookReservation.Book.Title} is available!" : string.Empty;
        }
    }
}

