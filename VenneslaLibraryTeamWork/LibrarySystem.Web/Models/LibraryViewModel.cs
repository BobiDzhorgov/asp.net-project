﻿using LibrarySystem.Contracts.Books;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibrarySystem.Web.Models
{
    public class LibraryViewModel : BaseViewModel
    {
        public List<BookViewModel> Books { get; set; }
        public LibraryViewModel(IEnumerable<IBook> books)
        {
            this.Books = new List<BookViewModel>();
            foreach (var book in books)
            {
                this.Books.Add(new BookViewModel(book));
            }
        }
    }
}
