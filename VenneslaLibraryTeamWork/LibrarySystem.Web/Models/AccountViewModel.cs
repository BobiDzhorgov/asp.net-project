﻿using LibrarySystem.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibrarySystem.Web.Models
{
    public class AccountViewModel : BaseViewModel
    {
        public AccountViewModel()
        {

        }
        public AccountViewModel(User user)
        {

            this.Username = user.UserName;
            this.LastNotification = user.LastNotification;
        }
        public string Username { get; set; }
    }
}
