﻿using LibrarySystem.Contracts.Books;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibrarySystem.Web.Models
{
    public class MessageViewModel : BaseViewModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
