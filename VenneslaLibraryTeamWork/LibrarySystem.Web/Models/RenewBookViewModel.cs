﻿using LibrarySystem.Contracts.Books;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibrarySystem.Web.Models
{
    public class RenewBookViewModel
    {
        public RenewBookViewModel(IBook book)
        {
            this.ISBN = book.ISBN;
        }
        public RenewBookViewModel()
        {

        }

        public string ISBN { get; set; }

        public int Days { get; set; }

    }
}
