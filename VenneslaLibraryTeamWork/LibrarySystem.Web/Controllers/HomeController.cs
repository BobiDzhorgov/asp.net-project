﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LibrarySystem.Web.Models;
using LibrarySystem.DbContext;
using Microsoft.EntityFrameworkCore;
using LibrarySystem.Web.Extensions;

namespace LibrarySystem.Web.Controllers
{
    public class HomeController : Controller
    {
        private LibrarySystemContext context;

        public HomeController(LibrarySystemContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            string lastNotification = string.Empty;
            if (User.Identity.IsAuthenticated)
            {
                context.SyncNotifications(User.Identity.Name);
                lastNotification = context.Notifications.GetLastNotification(User.Identity.Name);
            }

            return View(new BaseViewModel { LastNotification = lastNotification });
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
