﻿using LibrarySystem.DbContext;
using LibrarySystem.Services.Core;
using LibrarySystem.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace LibrarySystem.Web.Controllers
{
    public class BookController : Controller
    {

        private readonly IBookWebService service;

        public BookController(IBookWebService service, LibrarySystemContext context)
        {
            this.service = service;
        }
        [HttpGet]
        [Authorize]
        public IActionResult SearchBook()
        {
            return View();
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> SearchBook(string title)
        {
            var book = await this.service.SearchBook(title);
            if (book == null)
            {
                return View("Message", new MessageViewModel { Message = "The book was not found!" });
            }
            var bookModel = new BookViewModel(book);
            return View("Search", bookModel);
        }


        public async Task<IActionResult> Details(int id)
        {
            var book = await this.service.FindBook(id);
            if (book == null)
            {
                return View("Message", new MessageViewModel { Message = "The book was not found!" });
            }


            return View(book);
        }

        public async Task<IActionResult> BookInfo()
        {
            var books = await this.service.GetAllBooks();

            var libraryViewModel = new LibraryViewModel(books);

            return View(libraryViewModel);
        }

        [HttpGet]
        [Authorize]
        public IActionResult AddBook()
        {
            return View();
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> AddBook(BookViewModel book)
        {
            if (!ModelState.IsValid)
            {
                return View("Message", new MessageViewModel { Message = "The provided data is valid!" });
            }
            await this.service.CreateBookAsync(book.ISBN, book.Title, book.Pages, book.Subject, book.Publishers, book.PublishDate, book.Author);

            return RedirectToAction("BookInfo", "Book");
        }
        [HttpGet]
        [Authorize]
        public IActionResult RemoveBook()
        {
            return View();
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> RemoveBook(BookViewModel vm)
        {
            try
            {
                await this.service.RemovedBookAsync(vm.ISBN);
            }
            catch (Exception ex)
            {
                return View("Message", new MessageViewModel { Message = ex.Message });
            }

            return RedirectToAction("BookInfo", "Book");
        }
        [HttpGet]
        [Authorize]
        public IActionResult LendBook()
        {
            return View();
        }
        [HttpPost]
        [Authorize]
        public IActionResult LendBook(BookViewModel vm)
        {
            try
            {
                this.service.LendBook(vm.Title, User.Identity.Name);
            }
            catch (Exception ex)
            {
                return View("Message", new MessageViewModel { Message = ex.Message });
            }

            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        [Authorize]
        public IActionResult ReserveBook()
        {
            return View();
        }
        [HttpPost]
        [Authorize]
        public IActionResult ReserveBook(BookViewModel vm)
        {
            var result = this.service.ReserveBook(vm.Title, User.Identity.Name);
            if (!result)
            {
                return View("Message", new MessageViewModel { Message = "Book was not found!" });
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Authorize]
        public IActionResult ReturnBook()
        {
            return View();
        }


        [HttpPost]
        [Authorize]
        public IActionResult ReturnBook(BookViewModel vm)
        {
            try
            {
                this.service.ReturnBook(vm.ISBN, User.Identity.Name);
            }
            catch (Exception ex)
            {
                return View("Message", new MessageViewModel { Message = ex.Message });
            }

            return View("Message", new MessageViewModel { Message = "The book was returned successfully!", IsSuccess = true });
        }

        [HttpGet]
        [Authorize]
        public IActionResult RenewBook()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public IActionResult RenewBook(BookViewModel vm, int days)
        {
            try
            {
                this.service.RenewBook(vm.ISBN, User.Identity.Name, days);
            }
            catch (Exception ex)
            {
                return View("Message", new MessageViewModel { Message = ex.Message });
            }

            return View("Message", new MessageViewModel { Message = $"The book was renewed successfully for {days} days!", IsSuccess = true });
        }
    }
}