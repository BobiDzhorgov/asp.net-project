﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibrarySystem.Services.Core;
using LibrarySystem.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LibrarySystem.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountWebService service;

        public AccountController(IAccountWebService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Authorize]
        public IActionResult BannedUsers()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public IActionResult BannedUsers(AccountViewModel user)
        {

            try
            {
                var username = this.service.BanAccount(user.Username);

            }
            catch(Exception ex)
            {
                return View("Message", new MessageViewModel { Message = ex.Message });
            }

            return View("Message", new MessageViewModel { Message = $"User with username" +
                $" {user.Username} has been banned" , IsSuccess = true });
        }
    }
}